

## Introduction 

The goal of the project is, given a sequence of images, to recognize and classify those images based on
different possible categories including images that, developed with CXR, represent various different types
of COVID-19 infections, various viral phenomena and images of healthy patients. Based on this, the approach proposed by this research focuses on the use of neural networks of
convolution which will analyze the images improving their classification even with high
dimensions of the data and phenomena that can deteriorate their quality (presence of noise or poor robustness of the
datasets).

![Serial radiological progression](f1.png)

During the development of the project, the ease of such a problem for a neural network was immediately highlighted
convolution with no more than a single layer of output and input with an attached Flatten fiction for addition
some elements as part of the columns and some convolution layer and some dedicated to the features
extraction for the optimization and reduction of the complexity of MAC operations.
In addition to the simple neural network, a convolution layer has therefore been added which makes use of the
techniques glimpsed in class for the analysis of these data and a sequence of metrics was finally printed
related to the classification of samples (Matrices of confusion, Accuracy, False negative and true positive and so on
Street).
It was also necessary to verify the validity of the classification with the help of so-called validation
K-fold for the validation of the results obtained as well as the use of the metrics and techniques mentioned above e
above all a quick comparison with what is reported in the Covid-Net documentation.
The input data to the problem is in the data from my dataset, it must consist of different characteristics that
are readable and crucial to the classification for my algorithm based on neural networks with layers of
convolution.
As for the same execution of the neural network in test environments such as datasets such as
**COVIDx-CXR** , **COVIDx-CT**  or **COVIDx-S**  the various results in terms of sensitivity or accuracy of
classification were also compared with other neural networks developed for similar purposes perfect example of
this can be **VGG-19** or **ResNet-50**.
In these cases it was possible to find significant improvements not only in terms of precision of the
classification but also in terms of network complexity.

## Repo-Informations
Questo repository è stato creato al fine di fornire tutte le indformazioni relative il progetto richiesto per il superamento dell'esame
All'interno delle cartelle è possibile trovare tutta la documentazione e materiale a cui fa riferimento la documentazione e ovviamente il codice utilizzato nelle seguenti cartelle:                                                                                                      
1. **Relazione:** Contenente la relazione da me proposta e immagini contenute provenienti dalla documentazione di riferimento al progetto da me selezionato dove vi sono due versioni una pdf e una pages del file **relazione_Covid-Net**.
2. **Code:**
contenente il codice utilizzato, eventuali modelli partiicolarmente efficenti da me salvati in formato .h5 e i dataset di addestramento e traing Attenzione il dataset originale non è stato possibile caricarlo per via delle dimensioni troppo alte vedere file Original_dataset.png.
3. **Print_NN_test:**
Al cui interno è possible trovare varie stampe esemplificative del codice da me adottato.                                                             
4. **Resources:**
Utilizzata per contenere le risorse esterne alla documentazione di riferimento al progetto.
5. **presentazione:**
In questa sezione vi sarebbe dovuta essere la presntazione ma non posso caricare file di dimensioni troppo elevate.



Link riferimento:

- Paperswithcode:
https://paperswithcode.com/methods

- CXR images documentation for COVID-19:
https://www.bmj.com/content/370/bmj.m2426




-  Utilizzo di depthWiseConv2D e SeparableConv2D:
https://www.machinecurve.com/index.php/2019/09/24/creating-depthwise-separable-convolutions-in-keras/


- COVID-Net documentation:
https://paperswithcode.com/paper/covid-net-a-tailored-deep-convolutional

- Depth-wise convolutional layer:
https://towardsdatascience.com/a-basic-introduction-to-separable-convolutions-b99ec3102728

- Parallel Strategy for Convolutional Neural Network:
https://www.hindawi.com/journals/misy/2017/3824765/

- U-Net:Biomedical Convolutional Networks Image Segmentation:
https://link.springer.com/chapter/10.1007/978-3-319-24574-4_28

- A convolutional neural network in medical image analysis:
https://link.springer.com/article/10.1007/s11042-020-09634-7


- VGG-19
part1: https://towardsdatascience.com/transfer-learning-in-tensorflow-9e4f7eae3bb4                                                                  part2: https://towardsdatascience.com/transfer-learning-in-tensorflow-5d2b6ad495cb


- ResNet50:
https://towardsdatascience.com/deep-learning-using-transfer-learning-python-code-for-resnet50-8acdfb3a2d38
