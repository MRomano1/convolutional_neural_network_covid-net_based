#!/usr/bin/env python
# imports for array-handling and plotting
import numpy as np
import matplotlib
import struct
from skimage import color
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from tensorflow.keras import layers
import h5py
import os
from scipy.special import expit
from sklearn.model_selection import train_test_split, cross_val_score, ShuffleSplit, GridSearchCV
import sys
import pyttsx3
import progressbar
from sklearn.metrics import classification_report, confusion_matrix
from time import sleep
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
# keras imports for the dataset and building our neural network
from keras.preprocessing.image import ImageDataGenerator
from keras.datasets import mnist
from keras.models import Sequential, load_model
from keras.layers import Activation, Dropout, Flatten, Dense,Input
from keras.utils import np_utils
from keras.layers.convolutional import Conv2D,MaxPooling2D
from keras import backend as K
from sklearn.preprocessing import StandardScaler
from keras.layers.normalization import BatchNormalization
from keras import regularizers
from keras import optimizers
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
import tensorflow as tf
import tensorflow.keras as keras
from numpy import argmax
from keras.layers import Dense, Activation, Dropout, Flatten,Conv2D, MaxPooling2D, AveragePooling2D
from tensorflow.keras import initializers

img_width, img_height = 256, 256

data_dir = 'dataset1/train'
test_dir = 'dataset1/test'

batch_size = 320

nepochs = 10


print('====================LOADING DATASET===================');
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=113,
  image_size=(img_height, img_width),
  batch_size=batch_size)

train_datagen = ImageDataGenerator(
    rescale = 1. / 255)
test_datagen = ImageDataGenerator(
    rescale = 1. / 255)

val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  test_dir,
  validation_split=0.2,
  subset="validation",
  seed=18,
  image_size=(img_height, img_width),
  batch_size=batch_size)


val_ds2 = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="validation",
  seed=329,
  image_size=(img_height, img_width),
  batch_size=batch_size)

test_generator = test_datagen.flow_from_directory(
    test_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical',
    seed=142)
train_generator = train_datagen.flow_from_directory(
    data_dir,
    seed=113,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical')


(X_test, y_test) = test_generator.next()
(X_train, y_train) = train_generator.next()

class_names = train_ds.class_names
print(class_names)

print('====================SPLIT AND RESCALING===================');
normalization_layer = tf.keras.layers.experimental.preprocessing.Rescaling(1./255)
normalized_ds = train_ds.map(lambda x, y: (normalization_layer(x), y))
image_batch, labels_batch = next(iter(normalized_ds))


print('====================MODEL BASED ON CNN===================');
num_classes = 3
input_shape=X_train.shape[1:]
inp=tf.keras.Input(shape=input_shape)
layers.experimental.preprocessing.Rescaling(1./255),
#conv2d_1 = layers.Conv2D(56, (7,7),strides = (2,2), activation='relu', padding='same'),
#layers.MaxPooling2D((2,2)),
#conv2d_2 = layers.Conv2D(56, (5,5),strides = (2,2), activation='relu', padding='same'),
#layers.MaxPooling2D((2,2)),
#conv2d_3 = layers.Conv2D(56, (3,3),strides = (2,2), activation='relu', padding='same'),
#layers.MaxPooling2D((2,2)),


model = tf.keras.Sequential([
    layers.experimental.preprocessing.Rescaling(1./255),
    layers.Conv2D(56, (7,7),strides = (2,2), activation='relu', padding='same'),
    #layers.MaxPooling2D((2,2)),
    layers.Conv2D(56, (5,5),strides = (2,2), activation='relu', padding='same'),
    #layers.MaxPooling2D((2,2)),
    layers.Conv2D(56, (3,3),strides = (2,2), activation='relu', padding='same'),
    #layers.MaxPooling2D((2,2)),
    #layers.Concatenate()([conv2d_1, conv2d_2, conv2d_3]),
    #layers.Conv2D(424, 1, activation='relu'),
    #layers.MaxPooling2D((2,2)),
    #layers.Conv2D(400, 1, activation='relu'),
    #layers.MaxPooling2D((2,2)),
    layers.Conv2D(112,  (3, 3), activation='relu', padding='same'),
    layers.AveragePooling2D((2,2)),
    layers.Conv2D(56, (3, 3), activation='relu', padding='same'),
    #layers.DepthwiseConv2D((3,3), activation='relu'),
    layers.Conv2D(56,  (3, 3), activation='relu', padding='same'),
    layers.UpSampling2D((2,2)),
    layers.Conv2D(112,  (3, 3), activation='relu', padding='same'),
    layers.UpSampling2D((2,2)),
  #layers.Conv2D(400, 1, activation='relu'),
  #layers.UpSampling2D((2,2)),
  #layers.Conv2D(424, 1, activation='relu'),


    layers.Conv2D(224, (3, 3), activation='relu', padding='same'),
    layers.AveragePooling2D((2,2)),
    layers.Conv2D(112, (3, 3), activation='relu', padding='same'),
    layers.AveragePooling2D((2,2)),
    layers.Conv2D(56, (3, 3), activation='relu', padding='same'),
    #layers.DepthwiseConv2D((3,3), activation='relu'),
    layers.Conv2D(56, (3, 3), activation='relu', padding='same'),
    layers.UpSampling2D((2,2)),
    #layers.Conv2D(112, 1, activation='relu', padding='same'),
    #layers.UpSampling2D((2,2)),
    #layers.Conv2D(224, 1, activation='relu', padding='same'),


    #layers.Conv2D(424, 1, activation='relu', padding='same'),
    #layers.AveragePooling2D((2,2)),
    #layers.Conv2D(400, 1, activation='relu', padding='same'),
    #layers.MaxPooling2D((2,2)),
    layers.Conv2D(224, (3, 3), activation='relu', padding='same'),
    #layers.MaxPooling2D((2,2)),
    #layers.Conv2D(112, 1, activation='relu', padding='same'),
    #layers.DepthwiseConv2D((3,3), activation='relu'),
    #layers.Conv2D(112, 1, activation='relu', padding='same'),
    #layers.UpSampling2D((2,2)),
    layers.Conv2D(224, (3, 3), activation='relu', padding='same'),
    #layers.UpSampling2D((2,2)),
    #layers.Conv2D(400, 1, activation='relu', padding='same'),
    layers.UpSampling2D((2,2)),
    #layers.Conv2D(424, 1, activation='relu', padding='same'),


  layers.UpSampling2D((2,2)),
  #layers.Conv2D(56, (7,7),strides = (2,2), activation='relu'),
  #layers.Conv2D(112, (3,3),strides = (2,2), activation='relu'),
  layers.Conv2D(56, (3,3),strides = (2,2), activation='relu', padding='same'),
  layers.Flatten(),
  #layers.Dense(64,activation='relu'),
  #Dropout(0.5),
  layers.Dense(64,kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),activation='relu'),
  Dropout(0.5),
  layers.Dense(num_classes,activation = 'softmax')
  
])
model.compile(
  optimizer='adam',
  loss=tf.losses.SparseCategoricalCrossentropy(from_logits=True),
  metrics=['accuracy'])




"""====================FIT==================="""
print('FIT...');
history = model.fit(
  train_ds,
  validation_data=val_ds,
  epochs=nepochs
)
model.summary()
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']
"""
plt.figure(figsize=(8, 8))
plt.subplot(2, 1, 1)
plt.plot(acc, label='Training Accuracy')
plt.plot(val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.ylabel('Accuracy')
plt.ylim([min(plt.ylim()),1])
plt.title('Training and Validation Accuracy')

plt.subplot(2, 1, 2)
plt.plot(loss, label='Training Loss')
plt.plot(val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.ylabel('Cross Entropy')
plt.ylim([0,1.0])
plt.title('Training and Validation Loss')
plt.xlabel('epoch')
plt.show()
"""

print("==================PREDICTION==================");
y_test_p = [np.where(r==1)[0][0] for r in y_test]
print(X_test.shape,np.asarray(y_test_p).shape)
result,acc = model.evaluate(val_ds)
print("Accuracy", acc)
print("RES: ",result)

labels = np.array([])
#image = np.array([])

img, lab = next(iter(val_ds))
for l in lab:
    labels = np.append(labels,l.numpy())

print(labels.shape)
print(img.shape)
print("prediction...")
preds = model.predict(img,verbose=1)
y_pred = np.argmax(preds, axis=1)




print('====================RESULTS===================');
"""
fig, ax = plt.subplots(nrows=5, ncols=5, sharex=True, sharey=True,)
ax = ax.flatten()
for i in range(25):
    img = X_test[i]
    img = np.array(img, dtype='float')
    pixels = img.reshape((110, 110, 3))
    ax[i].imshow(pixels, cmap='Greys', interpolation='nearest')
ax[0].set_xticks([])
ax[0].set_yticks([])
plt.show()
"""


print("confusion matrix...")
print(classification_report(np.asarray(labels), y_pred, digits=3,target_names=class_names))
print(tf.math.confusion_matrix(labels=np.asarray(labels), predictions=y_pred).numpy())
y_pred = keras.utils.to_categorical(y_pred, 3)
"""
# summarize history for accuracy
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
"""

print('======================SAVE MODEL===================');
# saving the model
model_name = '1783252.h5'
model_path = os.path.join(model_name)
model.save(model_path)
print('Saved trained model at %s ' % model_path)

